// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "GlobalSpinner",
    products: [
        .library(name: "GlobalSpinner", type: .static, targets: ["GlobalSpinner"])
    ],
    targets: [
        .target(
            name: "GlobalSpinner",
            path: "GlobalSpinner"
        )
    ]
)


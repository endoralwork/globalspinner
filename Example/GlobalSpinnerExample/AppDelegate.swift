//
//  AppDelegate.swift
//  GlobalSpinnerExample
//
//  Created by Endoral on 02.08.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("\(AppDelegate.self): application(_:didFinishLaunchingWithOptions:)")
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("\(AppDelegate.self): applicationWillTerminate(_:)")
    }
}


//
//  SpinnerController.swift
//  GlobalSpinner
//
//  Created by Endoral on 12.07.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

import UIKit

final class SpinnerController: UIViewController {
    private let backgroundView = UIImageView()
    
    private let spinnerBackingView = UIImageView()
    
    private let spinnerView = UIImageView()
    
    private let activityIndicator: UIActivityIndicatorView = { () -> UIActivityIndicatorView in
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .whiteLarge)
        }
    }()
    
    var frames: Array<UIImage>?
    
    var duration: TimeInterval = 1.0
    
    var appearDuration: TimeInterval = 0.3
    
    var disappearDuration: TimeInterval = 0.2
    
    private(set) weak var cancelControl: UIControl?
    
    private var lastAction: ((Bool) -> Void)?
    
    private var lastWindowUserInteractionEnabledState: Bool? = nil
    
    private var viewFrame: CGRect = .zero
    
    private var layoutMode: GlobalSpinner.LayoutMode = .fullscreen
    
    private var layoutConstraints: Array<NSLayoutConstraint>?
    
    private var cancelAction: ((UIControl) -> Void)?
    
    private var spinnerActive: Bool = true
    
    var isActive: Bool {
        let result = !(view.isHidden || view.superview == nil || view.alpha == 0)
        
        return result
    }
    
    required init(frame: CGRect) {
        super.init(nibName: nil, bundle: nil)
        
        initialize()
        
        viewFrame = frame
        
        view.frame = frame
        
        view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        updateLayout(for: layoutMode)
    }
}
 
extension SpinnerController {
    func show(animated: Bool, blockWindow: Bool = false, spinnerActive: Bool = true, layoutMode: GlobalSpinner.LayoutMode = .fullscreen, completion: (() -> Void)? = nil) {
        if view.superview != nil && view.superview !== UIApplication.shared.currentKeyWindow {
            view.removeFromSuperview()
        }
        
        if view.superview == nil {
            UIApplication.shared.currentKeyWindow?.addSubview(view)
        }
        
        self.layoutMode = layoutMode
        self.spinnerActive = spinnerActive
        
        updateLayout(for: layoutMode)
        
        lastAction = showActionCompletion
        
        if let frames = frames {
            spinnerView.image = UIImage.animatedImage(with: frames, duration: duration)
        } else {
            if spinnerActive {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
        
        view.superview?.bringSubviewToFront(view)
        
        if blockWindow {
            if lastWindowUserInteractionEnabledState == nil {
                lastWindowUserInteractionEnabledState = view.window?.isUserInteractionEnabled
            }
            
            view.window?.isUserInteractionEnabled = false
        }
        
        backgroundView.alpha = 0
        
        view.isHidden = false
        
        UIView.animate(
            withDuration: animated
                ? appearDuration
                : 0.0,
            animations: { [weak self] in
                self?.backgroundView.alpha = 0.7
                self?.cancelControl?.isEnabled = false
            },
            completion: { [weak self] _ in
                if let strongSelf = self {
                    strongSelf.lastAction?(strongSelf.spinnerActive)
                    strongSelf.lastAction = nil
                }
                
                completion?()
            }
        )
    }
    
    func hide(animated: Bool, completion: (() -> Void)? = nil) {
        lastAction = hideActionCompletion
        
        UIView.animate(
            withDuration: animated
                ? disappearDuration
                : 0.0,
            animations: { [weak self] in
                self?.backgroundView.alpha = 0.0
                self?.cancelControl?.isEnabled = false
            },
            completion: { [weak self] _ in
                self?.lastAction?(false)
                self?.lastAction = nil
                
                completion?()
            }
        )
        
        spinnerView.image = nil
    }
    
    func startSpinnerAnimation() {
        if view.isHidden {
            return
        }
        
        if frames == nil {
            activityIndicator.startAnimating()
        } else {
            spinnerView.startAnimating()
        }
    }
    
    func stopSpinnerAnimation() {
        if view.isHidden {
            return
        }
        
        spinnerView.stopAnimating()
        activityIndicator.stopAnimating()
    }
    
    func deattach() {
        view.removeFromSuperview()
    }
    
    /// Attaches a copy of the referenced UIControl to the UIView of the GlobalSpinner
    /// - Parameters:
    ///     - control: UIControl for GlobalSpinner cancellation
    ///     - action: GlobalSpinner cancellation action
    func addCancelControl(from control: UIControl, event: UIControl.Event = .touchUpInside, action: ((UIControl) -> Void)? = nil) {
        guard cancelControl == nil else {
            #if DEBUG
                print("\(SpinnerController.self): Cancel control already exists")
            #endif
            
            return
        }
        
        guard let controlCopy = control.duplicate(with: [event]) else {
            #if DEBUG
                print("\(SpinnerController.self): Couldn't duplicate UIControl")
            #endif
            
            return
        }
        
        view.addSubview(controlCopy)
        
        cancelControl = controlCopy
        
        if let action = action {
            cancelAction = action
            
            cancelControl?.addTarget(self, action: #selector(cancelSelector(_:)), for: .touchUpInside)
        }
        
        let controlLeft = NSLayoutConstraint(
            item: controlCopy,
            attribute: .left,
            relatedBy: .equal,
            toItem: view,
            attribute: .left,
            multiplier: 1.0,
            constant: controlCopy.frame.origin.x
        )
        
        let controlTop = NSLayoutConstraint(
            item: controlCopy,
            attribute: .top,
            relatedBy: .equal,
            toItem: view,
            attribute: .top,
            multiplier: 1.0,
            constant: controlCopy.frame.origin.y
        )
        
        let controlWidth = NSLayoutConstraint(
            item: controlCopy,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .width,
            multiplier: 1.0,
            constant: control.bounds.width
        )
        
        let controlHeight = NSLayoutConstraint(
            item: controlCopy,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .height,
            multiplier: 1.0,
            constant: control.bounds.height
        )
        
        NSLayoutConstraint.activate([
            controlLeft,
            controlTop,
            controlWidth,
            controlHeight
        ])
    }
    
    /// Deattach cancellation's UIControl from the UIView of the GlobalSpinner
    func removeCancelControl() {
        cancelControl?.removeFromSuperview()
    }
}

private extension SpinnerController {
    func initialize() {
        spinnerBackingView.contentMode = .scaleAspectFit
        spinnerBackingView.translatesAutoresizingMaskIntoConstraints = false
        
        spinnerView.contentMode = .scaleAspectFit
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.color = .white
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.addSubview(spinnerBackingView)
        view.addSubview(spinnerView)
        view.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            spinnerView.centerXAnchor.constraint(equalTo: spinnerBackingView.centerXAnchor),
            spinnerView.centerYAnchor.constraint(equalTo: spinnerBackingView.centerYAnchor),
            spinnerView.heightAnchor.constraint(equalTo: spinnerBackingView.heightAnchor),
            spinnerView.widthAnchor.constraint(equalTo: spinnerBackingView.widthAnchor),
            spinnerBackingView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinnerBackingView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            spinnerBackingView.heightAnchor.constraint(equalToConstant: 40),
            spinnerBackingView.widthAnchor.constraint(equalTo: spinnerBackingView.heightAnchor)
        ])
    }
}
    
private extension SpinnerController {
    func updateLayout(for mode: GlobalSpinner.LayoutMode) {
        let layoutFrame = makeFrame(for: mode)
        let sizeConstraints = makeSizeConstraints(for: layoutFrame)
        
        layoutConstraints?.forEach { constraint in
            constraint.isActive = false
        }
        
        layoutConstraints = nil
        
        layoutConstraints = sizeConstraints
        
        NSLayoutConstraint.activate(sizeConstraints)
        
        view.frame.origin = layoutFrame.origin
    }
    
    func makeFrame(for mode: GlobalSpinner.LayoutMode) -> CGRect {
        let frame: CGRect
        
        switch mode {
        case .fullscreen:
            frame = UIScreen.main.bounds
            
        case .superframe:
            frame = view.superview?.bounds ?? viewFrame
            
        case .custom(frame: let customFrame):
            frame = customFrame
        }
        
        return frame
    }
    
    func makeSizeConstraints(for frame: CGRect) -> Array<NSLayoutConstraint> {
        let widthConstraint = NSLayoutConstraint(
            item: view!,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: frame.width
        )
        
        let heightConstraint = NSLayoutConstraint(
            item: view!,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: frame.height
        )
        
        let constraints = [
            widthConstraint,
            heightConstraint
        ]
        
        return constraints
    }
    
    func showActionCompletion(_ spinnerActive: Bool) {
        if spinnerActive {
            startSpinnerAnimation()
        } else {
            stopSpinnerAnimation()
        }
        
        cancelControl?.isEnabled = true
    }
    
    func hideActionCompletion(_: Bool) {
        stopSpinnerAnimation()
        
        view.isHidden = true
        
        cancelControl?.isEnabled = true
        
        if let windowUserInteractionEnabledState = lastWindowUserInteractionEnabledState {
            view.window?.isUserInteractionEnabled = windowUserInteractionEnabledState
            
            lastWindowUserInteractionEnabledState = nil
        }
    }
    
    @objc
    func cancelSelector(_ sender: UIControl) {
        cancelAction?(sender)
    }
}


//
//  UIControl_extension.swift
//  UIControl_extension
//
//  Created by Endoral on 13.11.2019.
//  Copyright © 2019 Endoral. All rights reserved.
//

import UIKit

extension UIControl {
    func duplicate(with controlEvents: [UIControl.Event]) -> UIControl? {
        let archivedControlData: Data
        let controlDuplicate: UIControl
        
        if #available(iOS 12.0, *) {
            do {
                try archivedControlData = NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: true)
            } catch {
                #if DEBUG
                    print("\(UIControl.self): Couldn't get archived data for control's duplicate\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
                #endif
                
                return nil
            }
            
            do {
                guard let unarchivedControl = try NSKeyedUnarchiver.unarchivedObject(ofClass: UIControl.self, from: archivedControlData) else {
                    return nil
                }
                
                controlDuplicate = unarchivedControl
            } catch {
                #if DEBUG
                    print("\(UIControl.self): Couldn't get unarchived object from data for \(UIControl.self) class\n - Reason: \(error.localizedDescription)\n - Error: \(error)")
                #endif
                
                return nil
            }
        } else {
            archivedControlData = NSKeyedArchiver.archivedData(withRootObject: self)
            
            guard let unarchivedControlData = NSKeyedUnarchiver.unarchiveObject(with: archivedControlData) else {
                return nil
            }
            
            guard let unarchivedControl = unarchivedControlData as? UIControl else {
                return nil
            }
            
            controlDuplicate = unarchivedControl
        }
        
        allTargets.forEach { target in
            controlEvents.forEach { controlEvent in
                actions(forTarget: target, forControlEvent: controlEvent)?.forEach { action in
                    controlDuplicate.addTarget(target, action: Selector(action), for: controlEvent)
                }
            }
        }
        
        return controlDuplicate
    }
}










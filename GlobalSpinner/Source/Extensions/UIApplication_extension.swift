//
//  UIApplication_extension.swift
//  UIApplication_extension
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import UIKit

extension UIApplication {
    var currentKeyWindow: UIWindow? {
        let keyWindow: UIWindow?
        
        if #available(iOS 13.0, *) {
            let windows = UIApplication.shared.connectedScenes.filter { scene in
                scene.activationState == .foregroundActive
            }.compactMap { scene in
                scene as? UIWindowScene
            }.first?.windows ?? UIApplication.shared.windows
            
            keyWindow = windows.filter { window in
                window.isKeyWindow
            }.first
        } else {
            keyWindow = UIApplication.shared.keyWindow
        }
        
        return keyWindow
    }
    
    var currentStatusBarFrame: CGRect? {
        if #available(iOS 13.0, *) {
            return currentKeyWindow?.windowScene?.statusBarManager?.statusBarFrame
        } else {
            return UIApplication.shared.statusBarFrame
        }
    }
}

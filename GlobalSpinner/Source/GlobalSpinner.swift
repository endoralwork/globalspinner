//
//  GlobalSpinner.swift
//  GlobalSpinner
//
//  Created by Endoral on 05.05.17.
//  Copyright © 2017 Endoral. All rights reserved.
//

import UIKit

/// Global spinner to prevent any actions in the application at the time of its display.
public final class GlobalSpinner {
    private var spinnerControllers: Dictionary<String, SpinnerController>?
    
    private init() {
        spinnerControllers = nil
    }
}

extension GlobalSpinner {
    public static let defaultId: String = "\(GlobalSpinner.self)_default"
    
    public static let shared = GlobalSpinner()
    
    /// Framing from the sprite sheet.
    /// - Parameters:
    ///     - spriteSheet: Sprite sheet with custom animation frames for spinner.
    /// - Returns: An array of custom animation frames for a spinner.
    public static func cutIntoFrames(spriteSheet: UIImage) -> [UIImage] {
        let minRatio = min(spriteSheet.size.width, spriteSheet.size.height)
        let maxRatio = max(spriteSheet.size.width, spriteSheet.size.height)
        let framesCount = Int(maxRatio / minRatio)
        let frameSize = minRatio
        let vertical = spriteSheet.size.width < spriteSheet.size.height
        
        let slicedFrames = (0 ..< framesCount).compactMap { multiplier -> UIImage? in
            let x: CGFloat
            let y: CGFloat
            
            if vertical {
                x = 0
                y = frameSize * CGFloat(multiplier)
            } else {
                x = frameSize * CGFloat(multiplier)
                y = 0
            }
            
            let frameRect = CGRect(
                x: x,
                y: y,
                width: frameSize,
                height: frameSize
            )
            
            guard let cgFrame = spriteSheet.cgImage?.cropping(to: frameRect) else {
                return nil
            }
            
            let uiFrame = UIImage(cgImage: cgFrame)
            
            return uiFrame
        }
        
        return slicedFrames
    }
}
 
extension GlobalSpinner {
    /// Remove view of spinner controller from its superview.
    public func deattach(for id: String = GlobalSpinner.defaultId) {
        let removedSpinnerController = spinnerControllers?.removeValue(forKey: id)
        
        if spinnerControllers?.isEmpty ?? false {
            spinnerControllers = nil
        }
        
        removedSpinnerController?.deattach()
    }
    
    /// Show GlobalSpinner over current application's key window.
    /// - Parameters:
    ///     - animated: Show with animated appearance.
    ///     - spinnerActive: Animate spinner, by default 'true'.
    ///     - layoutMode: Layout mode for determining the size and position of the spinner frame.
    ///     - completion: Executed after the animation ends. If there is no animation, then it is executed instantly.
    public func show(
        for id: String = GlobalSpinner.defaultId,
        animated: Bool,
        blockWindow: Bool = false,
        spinnerActive: Bool = true,
        layoutMode: GlobalSpinner.LayoutMode = .fullscreen,
        completion: (() -> Void)? = nil
    ) {
        let spinnerController: SpinnerController
        
        if let existingSpinnerController = spinnerControllers?[id] {
            spinnerController = existingSpinnerController
        } else {
            let newSpinnerController = SpinnerController(frame: UIScreen.main.bounds)
            
            if spinnerControllers == nil {
                spinnerControllers = [id: newSpinnerController]
            } else {
                spinnerControllers?[id] = newSpinnerController
            }
            
            spinnerController = newSpinnerController
        }
        
        spinnerController.show(
            animated: animated,
            blockWindow: blockWindow,
            spinnerActive: spinnerActive,
            layoutMode: layoutMode,
            completion: completion
        )
    }
    
    /// Hide GlobalSpinner.
    /// - Parameters:
    ///     - animated: Show fade animation.
    ///     - completion: Executed after the animation ends. If there is no animation, then it is executed instantly.
    public func hide(
        for id: String = GlobalSpinner.defaultId,
        animated: Bool,
        completion: (() -> Void)? = nil
    ) {
        spinnerControllers?[id]?.hide(
            animated: animated,
            completion: completion
        )
    }
    
    /// Start playing spinner animation.
    /// - Parameters:
    ///     - withDuration: Duration of custom spinner animation, by default '1.0'.
    public func startSpinnerAnimation(
        for id: String = GlobalSpinner.defaultId,
        with duration: TimeInterval = 1.0
    ) {
        guard let spinnerController = spinnerControllers?[id] else {
            return
        }
        
        spinnerController.duration = duration
        
        spinnerController.startSpinnerAnimation()
    }
    
    /// Stop playing spinner animation.
    public func stopSpinnerAnimation(for id: String = GlobalSpinner.defaultId) {
        spinnerControllers?[id]?.stopSpinnerAnimation()
    }
    
    /// Attaches a copy of the referenced UIControl to the UIView of the GlobalSpinner.
    /// - Parameters:
    ///     - from: UIControl for GlobalSpinner cancellation.
    ///     - event: Event type of UIControl reaction.
    ///     - action: GlobalSpinner cancellation action.
    public func addCancelControl(
        for id: String = GlobalSpinner.defaultId,
        from control: UIControl,
        event: UIControl.Event = .touchUpInside,
        action: ((UIControl) -> Void)? = nil
    ) {
        spinnerControllers?[id]?.addCancelControl(
            from: control,
            event: event,
            action: action
        )
    }
    
    /// Deattach cancellation's UIControl from the UIView of the GlobalSpinner.
    public func removeCancelControl(for id: String = GlobalSpinner.defaultId) {
        spinnerControllers?[id]?.removeCancelControl()
    }
}

extension GlobalSpinner {
    /// Frames for custom spinner animation.
    public func frames(for id: String = GlobalSpinner.defaultId) -> Array<UIImage>? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.frames
    }
    
    /// Duration of custom spinner animation.
    public func duration(for id: String = GlobalSpinner.defaultId) -> TimeInterval? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.duration
    }
    
    /// GlobalSpinner appearance duration.
    public func appearDuration(for id: String = GlobalSpinner.defaultId) -> TimeInterval? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.appearDuration
    }
    
    /// GlobalSpinner disappearance duration.
    public func disappearDuration(for id: String = GlobalSpinner.defaultId) -> TimeInterval? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.disappearDuration
    }
    
    /// Current GlobalSpinner activity status.
    public func isActive(for id: String = GlobalSpinner.defaultId) -> Bool? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.isActive
    }
    
    /// UIControl for GlobalSpinner's background action cancellation.
    public func cancelControl(for id: String = GlobalSpinner.defaultId) -> UIControl? {
        guard let spinnerController = spinnerControllers?[id] else {
            return nil
        }
        
        return spinnerController.cancelControl
    }
}

extension GlobalSpinner {
    public func set(
        frames: Array<UIImage>?,
        for id: String = GlobalSpinner.defaultId
    ) {
        guard let spinnerController = spinnerControllers?[id] else {
            return
        }
        
        spinnerController.frames = frames
    }
    
    public func set(
        duration: TimeInterval,
        for id: String = GlobalSpinner.defaultId
    ) {
        guard let spinnerController = spinnerControllers?[id] else {
            return
        }
        
        spinnerController.duration = duration
    }
    
    public func set(
        appearDuration duration: TimeInterval,
        for id: String = GlobalSpinner.defaultId
    ) {
        guard let spinnerController = spinnerControllers?[id] else {
            return
        }
        
        spinnerController.appearDuration = duration
    }
    
    public func set(
        disappearDuration duration: TimeInterval,
        for id: String = GlobalSpinner.defaultId
    ) {
        guard let spinnerController = spinnerControllers?[id] else {
            return
        }
        
        spinnerController.disappearDuration = duration
    }
}

extension GlobalSpinner {
    /// Layout mode for determining the size and position of the spinner frame.
    public enum LayoutMode {
        /// Frame in absolute coordinates of the screen with a position of zero and the size of the full screen.
        case fullscreen
        
        /// Frame in the coordinates of the parent view with a position of zero and the size of the parent view.
        case superframe
        
        /// Custom frame size and position.
        case custom(frame: CGRect)
    }
}










//
//  GlobalSpinner.h
//  GlobalSpinner
//
//  Created by Endoral on 23.03.2018.
//  Copyright © 2018 Endoral. All rights reserved.
//

#import <UIKit/UIKit.h>

// GlobalSpinner version: 5.0.0

//! Project version number for GlobalSpinner.
FOUNDATION_EXPORT double GlobalSpinnerVersionNumber;

//! Project version string for GlobalSpinner.
FOUNDATION_EXPORT const unsigned char GlobalSpinnerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GlobalSpinner/PublicHeader.h>



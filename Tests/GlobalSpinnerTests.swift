//
//  GlobalSpinnerTests.swift
//  GlobalSpinnerTests
//
//  Created by Endoral on 02.08.2018.
//  Copyright © 2018 endoralwork-isi. All rights reserved.
//

import XCTest
import GlobalSpinner

class GlobalSpinnerTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test1Show() {
        GlobalSpinner.shared.show(animated: false)
        let result = GlobalSpinner.shared.isActive
        XCTAssertTrue(result)
    }
    
    func test1Hide() {
        GlobalSpinner.shared.hide(animated: false)
        let result = GlobalSpinner.shared.isActive
        XCTAssertFalse(result)
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}
